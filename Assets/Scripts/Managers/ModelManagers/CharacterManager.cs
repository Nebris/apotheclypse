﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager
{
    // Attributes
    private List<Character> characters;

    // Constructor
    public CharacterManager()
    {
        generateCharacterPrototypes();
    }

    // Generates all possible characters
    private void generateCharacterPrototypes()
    {
        characters = new List<Character>();

        characters.Add(new Character("George", ITaste.Bitter, ITaste.Salty, IColor.Blue, 2, IElement.Water));
        characters.Add(new Character("John", ITaste.Salty, ITaste.Sour, IColor.Orange, 1, IElement.Fire));
        characters.Add(new Character("Tom", ITaste.Sour, ITaste.Spicy, IColor.Red, -2, IElement.Earth));
        characters.Add(new Character("Jim", ITaste.Spicy, ITaste.Sweet, IColor.Purple, -1, IElement.Air));
        characters.Add(new Character("Ben", ITaste.Sweet, ITaste.Bitter, IColor.Green, -1, IElement.Water));
        characters.Add(new Character("Alex", ITaste.Sour, ITaste.Bitter, IColor.Yellow, 1, IElement.Fire));
    }

    // Returns copy of list of all characters
    public List<Character> getCharacters()
    {
        return new List<Character>(characters);
    }
}
