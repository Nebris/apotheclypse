﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientManager
{
    // Attributes
    private List<Ingredient> ingredients;

    // Constructor
    public IngredientManager()
    {
        generateIngredientPrototypes();
    }

    // Generates all possible ingredients
    private void generateIngredientPrototypes()
    {
        ingredients = new List<Ingredient>();

        for (int i = 0; i < 60; i ++)
        {
            int taste = (int)Random.Range(0, 5);
            int color = (int)Random.Range(0, 8);
            int charge = (int)Random.Range(-3, 4);
            int element = (int)Random.Range(0, 5);
            int count = (int)Random.Range(0, 4);

            ingredients.Add(new Ingredient("TestIngredient" + i, (ITaste)taste, (IColor)color, charge, (IElement)element, count));
        }
    }

    // Returns copy of list of all ingredients
    public List<Ingredient> getIngredients()
    {
        return new List<Ingredient>(ingredients);
    }
}
