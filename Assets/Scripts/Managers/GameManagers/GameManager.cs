﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // References
    private GameInfo gameInfo;

    // Attributes
    public GameIngredientManager gameIngredientManager { get; protected set; }
    public GamePlayerManager gamePlayerManager { get; protected set; }
    public GameRoundManager gameRoundManager { get; protected set; }

    public PlayerObjectManager playerObjectManager { get; protected set; }
    public IngredientObjectManager ingredientObjectManager { get; protected set; }
    public PotionObjectManager potionObjectManager { get; protected set; }

    private bool notInRound = true;
    private bool turnOver = false;
    private float wait = 3f;

    // Use this for initialization
    void OnEnable()
    {
        gameInfo = MasterManager.Instance.gameInfo;
        gameInfo.logGameInfo();

        gameIngredientManager = new GameIngredientManager();
        gamePlayerManager = new GamePlayerManager(this);
        gameRoundManager = new GameRoundManager(this);

        playerObjectManager = gameObject.AddComponent<PlayerObjectManager>();
        ingredientObjectManager = gameObject.AddComponent<IngredientObjectManager>();
        potionObjectManager = gameObject.AddComponent<PotionObjectManager>();

        gameObject.AddComponent<ButtonObjectManager>();
        gameObject.AddComponent<TextObjectManager>();
    }

    void Start()
    {
        gamePlayerManager.generatePlayers();
        potionObjectManager.generatePotionObjects();
    }

    void Update()
    {
        if (notInRound)
        {
            if (wait >= 0)
            {
                wait -= Time.deltaTime;
            }
            else
            {
                notInRound = false;
                gameRoundManager.startNextRound();
            }
        }

        if (turnOver)
        {
            if (wait >= 0)
            {
                wait -= Time.deltaTime;
            }
            else
            {
                turnOver = false;
                gameRoundManager.currentRound.nextTurn();
            }
        }
    }

    public void onRoundOver()
    {
        notInRound = true;
        wait = 5f;
    }

    // Player adds an ingredient to the community potion and their turn ends
    public void addIngredientToComunityPotion(Player player, Ingredient ingredient)
    {
        if (isNotPlayerTurn(player))
        {
            Debug.Log("Not your turn, " + player.name + " !");
            return;
        }

        Debug.Log(player.name + " adds " + ingredient.name + " to the community potion.");
        //ingredient.logIngredient();

        player.removeIngredient(ingredient);
        gameRoundManager.currentRound.communityPotion.addIngredient(ingredient);
        gameIngredientManager.addUsedIngredient(ingredient);

        //gameRoundManager.currentRound.communityPotion.logPotionAttributes();
        
        onTurnTaken();
    }

    // Player adds an ingredient to their personal potion and their turn ends
    public void addIngredientToPersonalPotion(Player player, Ingredient ingredient)
    {
        if (isNotPlayerTurn(player))
        {
            Debug.Log("Not your turn, " + player.name + " !");
            return;
        }

        Debug.Log(player.name + " adds " + ingredient.name + " to the personal potion.");
        //ingredient.logIngredient();

        player.removeIngredient(ingredient);
        player.potion.addIngredient(ingredient);
        gameIngredientManager.addUsedIngredient(ingredient);

        //gameRoundManager.currentRound.currentTurn.player.potion.logPotionAttributes();

        onTurnTaken();
    }

    // Player drinks from their personal potion and their turn ends
    public void drinkPlayerPotion(Player player)
    {
        if (isNotPlayerTurn(player))
        {
            Debug.Log("Not your turn, " + player.name + " !");
            return;
        }

        Debug.Log(player.name + " drinks their potion");

        player.assessPotionDamage();

        onTurnTaken();
    }

    public void drinkControlledPlayerPotion()
    {
        drinkPlayerPotion(gamePlayerManager.controlledPlayer);
    }

    private void onTurnTaken()
    {
        turnOver = true;
        wait = 1f;
    }

    public bool isNotPlayerTurn(Player player)
    {
        return (turnOver || gameRoundManager.currentRound == null 
            || gameRoundManager.currentRound.currentTurn == null 
            || gameRoundManager.currentRound.currentTurn.player != player);
    }
}
