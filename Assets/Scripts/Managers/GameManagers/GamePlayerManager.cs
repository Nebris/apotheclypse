﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayerManager 
{
    // References
    private GameManager gameManager;
    private CharacterManager characterManager;
    private GameInfo gameInfo;

    // Attributes
    public Player controlledPlayer { get; protected set; }

    private List<Player> players;
    private List<Character> potentialCharacters;

    private int buttonIndex;
    
    // Callbacks
    Action<Player> playerCreatedCallback;
    Action<Player> controlledPlayerCreatedCallback;

    // Use this for initialization
    public GamePlayerManager(GameManager gameManager)
    {
        this.gameManager = gameManager;
        characterManager = MasterManager.Instance.characterManager;
        gameInfo = MasterManager.Instance.gameInfo;

        potentialCharacters = characterManager.getCharacters();
    }
    
    // Advances the button (player who goes first)
    public void advanceButton()
    {
        buttonIndex++;

        if (buttonIndex >= players.Count)
        {
            buttonIndex = 0;
        }
    }
    
    // Generates a list of players for our game
    public void generatePlayers()
    {
        players = new List<Player>();

        for (int p = 0; p < gameInfo.players; p++)
        {
            Player player = new Player();

            players.Add(player);

            if (gameInfo.gameMode == GameMode.Elimination)
            {
                player.setCharacter(getUnusedCharacter());
                player.setName(player.character.name);

                player.setPoints(gameInfo.points);
            }
            else if (gameInfo.gameMode == GameMode.Quest)
            {
                //get quest card

                player.setPoints(0);
            }
            
            if (p == 0)
            {
                controlledPlayer = player;
                if (controlledPlayerCreatedCallback != null) controlledPlayerCreatedCallback(controlledPlayer);
                Debug.Log("You are " + controlledPlayer.name);
            }

            for (int i = 0; i < gameInfo.ingredients; i++)
            {
                player.addIngredient(gameManager.gameIngredientManager.getUnusedIngredient());
            }

            //player.logIngredients();

            if (playerCreatedCallback != null)
            {
                playerCreatedCallback(player);
            }
        }

        buttonIndex = (int)UnityEngine.Random.Range(0, players.Count);
        Debug.Log(players[buttonIndex].name + " goes first!");
    }

    // Returns a copy of players
    public List<Player> getPlayers()
    {
        return new List<Player>(players);
    }

    // Returns a list of players who will be participating in the next round
    public List<Player> getPlayersForRound()
    {
        List<Player> first = new List<Player>();
        List<Player> second = new List<Player>();
        List<Player> roundPlayers = new List<Player>();

        for (int i = 0; i < players.Count; i++)
        {
            if (gameInfo.gameMode == GameMode.Elimination && players[i].points <= 0) continue;

            if (i < buttonIndex)
            {
                second.Add(players[i]);
            }
            else
            {
                first.Add(players[i]);
            }
        }

        roundPlayers.AddRange(first);
        roundPlayers.AddRange(second);

        return roundPlayers;
    }

    // Selects a random unused character to make a player out of
    private Character getUnusedCharacter()
    {
        int index = (int)UnityEngine.Random.Range(0, potentialCharacters.Count);
        Character character = potentialCharacters[index];
        potentialCharacters.RemoveAt(index);

        return character;
    }

    // Restores player inventories to full
    public void restorePlayerInventories()
    {
        foreach (Player player in players)
        {
            if (gameInfo.gameMode == GameMode.Elimination && player.points <= 0) continue;

            for(int i = 0; i < (gameInfo.ingredients + 1 - player.getIngredientsCount()); i++)
            {
                player.addIngredient(gameManager.gameIngredientManager.getUnusedIngredient());
            }
        }
    }

    // Callback for when a player is created
    public void registerPlayerCreatedCallback(Action<Player> callback)
    {
        playerCreatedCallback += callback;
    }

    public void unregisterPlayerCreatedCallback(Action<Player> callback)
    {
        playerCreatedCallback -= callback;
    }

    // Callback for when the controlled player is created
    public void registerControlledPlayerCreatedCallback(Action<Player> callback)
    {
        controlledPlayerCreatedCallback += callback;
    }

    public void unregisterControlledPlayerCreatedCallback(Action<Player> callback)
    {
        controlledPlayerCreatedCallback -= callback;
    }
}
