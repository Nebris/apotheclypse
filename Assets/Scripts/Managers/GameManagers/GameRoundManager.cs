﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRoundManager 
{
    // References
    private GameManager gameManager;

    // Attributes
    public Round currentRound { get; protected set; }

    private List<Round> rounds;

    // Callbacks
    private Action<Round> roundChangedCallback;
    private Action<Player> winnerDecidedCallback;

    // Constructor
    public GameRoundManager(GameManager gameManager)
    {
        this.gameManager = gameManager;

        rounds = new List<Round>();
    }

    // Ends the current round
    public void onRoundOver()
    {
        Debug.Log("End of current round");

        List<Player> players = gameManager.gamePlayerManager.getPlayersForRound();

        foreach (Player player in players)
        {
            player.emptyPotion();
        }

        if (players.Count == 1)
        {
            //Winner
            Debug.Log(players[0].name + " wins!");
            if (winnerDecidedCallback != null) winnerDecidedCallback(players[0]);
        }
        else
        {
            if (players.Count == 0)
            {
                //There was a tie!!!
                foreach (Player player in currentRound.getPlayers())
                {
                    player.setPoints(Math.Min(MasterManager.Instance.gameInfo.points, 3));
                }
            }

            gameManager.gamePlayerManager.restorePlayerInventories();
            gameManager.gamePlayerManager.advanceButton();
            gameManager.onRoundOver();
        } 
    }

    // Creates a new round to be played
    public void startNextRound()
    {
        Debug.Log("Starting new round!");
        currentRound = new Round(gameManager.gamePlayerManager.getPlayersForRound());

        //gameManager.potionInfoManager.potion = currentRound.communityPotion;
        rounds.Add(currentRound);
        currentRound.registerRoundOverCallback(onRoundOver);

        if (roundChangedCallback != null) roundChangedCallback(currentRound);

        currentRound.begin();
    }

    // Callback for when a new round begins
    public void registerRoundChangedCallback(Action<Round> callback)
    {
        roundChangedCallback += callback;
    }

    public void unregisterRoundChangedCallback(Action<Round> callback)
    {
        roundChangedCallback -= callback;
    }

    // Callback for when the winner has been decided
    public void registerWinnerDecidedCallback(Action<Player> callback)
    {
        winnerDecidedCallback += callback;
    }

    public void unregisterWinnerDecidedCallback(Action<Player> callback)
    {
        winnerDecidedCallback -= callback;
    }
}
