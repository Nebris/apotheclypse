﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameIngredientManager
{
    // References
    private IngredientManager ingredientManager;

    // Attributes
    private List<Ingredient> unusedIngredients;
    private List<Ingredient> usedIngredients;

    // Constructor
    public GameIngredientManager()
    {
        ingredientManager = MasterManager.Instance.ingredientManager;

        unusedIngredients = ingredientManager.getIngredients();
        usedIngredients = new List<Ingredient>();
    }

    // Adds a used ingredient to our list of ued ingredients to be recycled
    public void addUsedIngredient(Ingredient ingredient)
    {
        usedIngredients.Add(ingredient);
    }

    // Returns a random ingredient from our list of unused ingredients (removing it from our list of ingredients)
    public Ingredient getUnusedIngredient()
    {
        if (unusedIngredients.Count < 1)
        {
            Debug.Log("Shuffling in used ingredients.");

            unusedIngredients.AddRange(usedIngredients);
            usedIngredients = new List<Ingredient>();
        }

        int index = (int)UnityEngine.Random.Range(0, unusedIngredients.Count);
        Ingredient ingredient = unusedIngredients[index];
        unusedIngredients.RemoveAt(index);

        return ingredient;
    }
}
