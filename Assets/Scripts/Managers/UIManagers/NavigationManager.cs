﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavigationManager : MonoBehaviour 
{
    public void mainMenu()
    {
        Debug.Log("Main Menu");

        SceneManager.LoadScene("Main");
    }

    public void playGame()
    {
        Debug.Log("New Game");

        SceneManager.LoadScene("Game");
    }

    public void exitGame()
    {
        Application.Quit();
    }
}
