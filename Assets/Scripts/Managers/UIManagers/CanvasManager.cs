﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    // Ingredient Transforms
    public Transform ingredientsCanvasTransform { get; protected set; }
    public Transform playerIngredientsTransform { get; protected set; }
    public Transform potionIngredientsTransform { get; protected set; }

    // Player Transforms
    private List<Transform> playerSlotTransforms;
    private int playerSlotIndex;

    // Potion Transforms
    public Transform communityPotionTransform { get; protected set; }
    public Transform playerPotionTransform { get; protected set; }

    // GameText Transform
    public Transform gameTextCanvas { get; protected set; }

    void OnEnable()
    {
        ingredientsCanvasTransform = GameObject.Find("IngredientsCanvas").transform;
        playerIngredientsTransform = GameObject.Find("PlayerIngredients").transform;
        potionIngredientsTransform = GameObject.Find("PotionIngredients").transform;

        playerSlotTransforms = new List<Transform>();
        playerSlotTransforms.Add(GameObject.Find("PlayerSlot 1").transform);
        playerSlotTransforms.Add(GameObject.Find("PlayerSlot 2").transform);
        playerSlotTransforms.Add(GameObject.Find("PlayerSlot 3").transform);
        playerSlotTransforms.Add(GameObject.Find("PlayerSlot 4").transform);
        playerSlotIndex = 0;
        
        communityPotionTransform = GameObject.Find("CommunityPotion").transform;
        playerPotionTransform = GameObject.Find("PlayerPotion").transform;

        gameTextCanvas = GameObject.Find("GameTextCanvas").transform;
    }

    public Transform getNextPlayerSlot()
    {
        if (playerSlotIndex >= playerSlotTransforms.Count)
        {
            Debug.LogError("Out of player slots!");
            return null;
        }

        Transform playerSlotTransform = playerSlotTransforms[playerSlotIndex];
        playerSlotIndex++;

        return playerSlotTransform;
    }
}
