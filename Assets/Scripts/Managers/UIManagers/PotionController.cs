﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotionController : MonoBehaviour
{
    // References
    private Image potionImage;

    // Attributes
    private Potion potion;

    private Transform desiredLocation;
    private float desiredScale;

    private bool atDesiredLocation;
    private bool atDesiredScale;

    private float speed = 0.05f;
    private float locationPercent = 0f;
    private float scalePercent = 0f;
    private Vector3 startingLocation;
    private Vector3 startingScale;

    // Use this for initialization
    void OnEnable()
    {
        potionImage = gameObject.GetComponent<Image>();

        if (potionImage == null) Debug.LogError("Potion lacking image.");

        atDesiredLocation = true;
        atDesiredScale = true;
    }

    void Update()
    {
        if (atDesiredLocation == false)
        {
            locationPercent += Time.deltaTime + speed;

            if ( locationPercent >= 1)
            {
                atDesiredLocation = true;
                transform.SetParent(desiredLocation);
                transform.SetAsFirstSibling();
                transform.localPosition = Vector3.zero;
            }
            else
            {
                transform.position = Vector3.Lerp(startingLocation, desiredLocation.position, locationPercent);
            }
        }

        if (atDesiredScale == false)
        {
            scalePercent += Time.deltaTime + speed;

            if (scalePercent >= 1)
            {
                atDesiredScale = true;
                transform.localScale = Vector3.one * desiredScale;
            }
            else
            {
                transform.localScale = Vector3.Lerp(startingScale, Vector3.one * desiredScale, scalePercent);
            }
        }
    }

    // Callback for when the potion has been altered
    public void onPotionChanged(Ingredient ingredient)
    {
        if (potion == null)
        {
            potionImage.color = Color.white;
            return;
        }

        potionImage.color = ColorUtils.getColor(potion.getPotionColor());
    }

    // Changes the desired location of our potion object
    public void setDesiredLocation(Transform desiredLocation)
    {
        this.desiredLocation = desiredLocation;
        startingLocation = transform.position;
        locationPercent = 0f;
        atDesiredLocation = false;
    }

    // Changes the desired scale of our potion object
    public void setDesiredScale(float desiredScale)
    {
        this.desiredScale = desiredScale;
        startingScale = transform.localScale;
        scalePercent = 0f;
        atDesiredScale = false;
    }

    // Changes the potion we keep track of
    public void setPotion(Potion potion)
    {
        if (this.potion != null) this.potion.unregisterIngredientAddedCallback(onPotionChanged);
        
        this.potion = potion;

        if (potion != null)
        {
            potion.registerIngredientAddedCallback(onPotionChanged);
        }
        onPotionChanged(null);
    }
}
