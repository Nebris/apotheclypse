﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOptionsManager : MonoBehaviour 
{
    // References
    private GameInfo gameInfo;

    private GameObject optionsPanel;

    private Dropdown gameModeDropDown;
    private Dropdown playerCardDropDown;

    private Text playerCardText;
    private Text playersTextValue;
    private Text pointsText;
    private Text pointsTextValue;
    private Text ingredientsTextValue;

    private Slider playersSlider;
    private Slider pointsSlider;
    private Slider ingredientsSlider;

    // Use this for initialization
    void Start () 
	{
        gameInfo = MasterManager.Instance.gameInfo;

        optionsPanel = GameObject.Find("Options Panel");

        gameModeDropDown = GameObject.Find("Game Mode Dropdown").GetComponent<Dropdown>();
        playerCardDropDown = GameObject.Find("Player Card Dropdown").GetComponent<Dropdown>();

        playerCardText = GameObject.Find("Player Card Text").GetComponent<Text>();
        playersTextValue = GameObject.Find("Players Text Value").GetComponent<Text>();
        pointsText = GameObject.Find("Points Text").GetComponent<Text>();
        pointsTextValue = GameObject.Find("Points Text Value").GetComponent<Text>();
        ingredientsTextValue = GameObject.Find("Ingredients Text Value").GetComponent<Text>();

        playersSlider = GameObject.Find("Players Slider").GetComponent<Slider>();
        pointsSlider = GameObject.Find("Points Slider").GetComponent<Slider>();
        ingredientsSlider = GameObject.Find("Ingredients Slider").GetComponent<Slider>();

        if (optionsPanel == null)           Debug.LogError("optionsPanel was not found.");
        if (gameModeDropDown == null)       Debug.LogError("gameModeDropDown was not found.");
        if (playerCardDropDown == null)     Debug.LogError("playerCardDropDown was not found.");
        if (playerCardText == null)         Debug.LogError("playerCardText was not found.");
        if (playersTextValue == null)       Debug.LogError("playersTextValue was not found.");
        if (pointsText == null)             Debug.LogError("pointsText was not found.");
        if (pointsTextValue == null)        Debug.LogError("pointsTextValue was not found.");
        if (ingredientsTextValue == null)   Debug.LogError("ingredientsTextValue was not found.");
        if (playersSlider == null)          Debug.LogError("playersSlider was not found.");
        if (pointsSlider == null)           Debug.LogError("pointsSlider was not found.");
        if (ingredientsSlider == null)      Debug.LogError("ingredientsSlider was not found.");

        updateGameOptionsMenu();
        toggleOptionsPanel();
    }

    // Begins a new game
    public void playGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void toggleOptionsPanel()
    {
        optionsPanel.SetActive(!optionsPanel.activeInHierarchy);
    }

    // Updates the game info whenever a UI element value change occurs
    public void updateGameInfo()
    {
        if (gameModeDropDown.value == 0)
        {
            gameInfo.setGameMode(GameMode.Elimination);
            playerCardText.text = "Character Card";
            pointsText.text = "HP";
        }
        else if (gameModeDropDown.value == 1)
        {
            gameInfo.setGameMode(GameMode.Quest);
            playerCardText.text = "Quest Card";
            pointsText.text = "QP";
        }

        gameInfo.setPlayers((int) playersSlider.value);
        gameInfo.setPoints((int) pointsSlider.value);
        gameInfo.setIngredients((int) ingredientsSlider.value);

        playersTextValue.text = gameInfo.players.ToString();
        pointsTextValue.text = gameInfo.points.ToString();
        ingredientsTextValue.text = gameInfo.ingredients.ToString();

        //gameInfo.logGameInfo();
    }
	
    // Sets initial values of UI elements based off of current game info
	private void updateGameOptionsMenu()
    {
        switch (gameInfo.gameMode)
        {
            case GameMode.Elimination:
                gameModeDropDown.value = 0;
                playerCardText.text = "Character Card";
                pointsText.text = "HP";
                break;
            case GameMode.Quest:
                gameModeDropDown.value = 1;
                playerCardText.text = "Quest Card";
                pointsText.text = "QP";
                break;
        }

        //TODO don't always play random
        playerCardDropDown.value = 0;

        playersSlider.value = gameInfo.players;
        playersTextValue.text = gameInfo.players.ToString();

        pointsSlider.value = gameInfo.points;
        pointsTextValue.text = gameInfo.points.ToString();

        ingredientsSlider.value = gameInfo.ingredients;
        ingredientsTextValue.text = gameInfo.ingredients.ToString();
    }
}
