﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointBarController : MonoBehaviour
{
    // References
    private GameObject pointBar;
    private Text pointText;
    
	void OnEnable() 
	{
        pointBar = gameObject.transform.GetChild(0).gameObject;
        pointText = gameObject.GetComponentInChildren<Text>();
    }

    // Callback for when points are changed
    public void onPointsChanged(Player player, int difference)
    {
        float scale = player.points / (float)MasterManager.Instance.gameInfo.points;
        pointBar.transform.localScale = new Vector3(scale, 1, 1);

        pointText.text = player.points.ToString();
    }

    // Registers a player to this points bar
    public void setPlayer(Player player)
    {
        player.registerPointsChangedCallback(onPointsChanged);
        onPointsChanged(player, 0);
    }
}
