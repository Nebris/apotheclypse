﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonObjectManager : MonoBehaviour
{
    // References
    private GameManager gameManager;
    private GameObject drinkButtonObject;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();

        drinkButtonObject = GameObject.Find("DrinkPotionButton");
        if (drinkButtonObject == null) Debug.LogError("Could not find drinkButtonObject.");

        drinkButtonObject.GetComponent<Button>().onClick.AddListener(gameManager.drinkControlledPlayerPotion);
        drinkButtonObject.SetActive(false);

        gameManager.gameRoundManager.registerRoundChangedCallback(onRoundChange);
    }

    public void onRoundChange(Round round)
    {
        round.registerCurrentTurnChangedCallback(onTurnChange);
    }

    public void onTurnChange(Turn turn)
    {
        if(turn != null && turn.player == gameManager.gamePlayerManager.controlledPlayer && turn.phase == 3)
        {
            drinkButtonObject.SetActive(true);
        }
        else
        {
            drinkButtonObject.SetActive(false);
        }
    }
}
