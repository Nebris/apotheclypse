﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerObjectManager : MonoBehaviour
{
    // Prefabs
    private GameObject pointsBarPrefab;
    private GameObject playerTitleTextPrefab;

    // References
    private GameManager gameManager;
    private CanvasManager canvasManager;

    // Attributes
    private Dictionary<Player, GameObject> players;
    private Dictionary<Player, PlayerAI> playerAIs;

    private Round currentRound;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
        gameManager.gamePlayerManager.registerPlayerCreatedCallback(onPlayerCreated);

        canvasManager = GameObject.FindObjectOfType<CanvasManager>();

        pointsBarPrefab = (GameObject)Resources.Load("Prefabs/PointsBar");
        if (pointsBarPrefab == null) Debug.LogError("pointsBarPrefab failed to load");

        playerTitleTextPrefab = (GameObject)Resources.Load("Prefabs/PlayerTitleText");
        if (playerTitleTextPrefab == null) Debug.LogError("PlayerTitleText failed to load");

        players = new Dictionary<Player, GameObject>();
        playerAIs = new Dictionary<Player, PlayerAI>();

        currentRound = null;

        gameManager.gameRoundManager.registerRoundChangedCallback(onRoundChanged);
    }

    // Returns the player game object associated with passed player
    public GameObject getPlayerObject(Player player)
    {
        return players[player];
    }

    // Callback for when a player is created
    public void onPlayerCreated(Player player)
    {
        //Debug.Log(player.name + " object created.");

        Transform slotTransform = canvasManager.getNextPlayerSlot();

        GameObject playerTitle = GameObject.Instantiate(playerTitleTextPrefab);
        playerTitle.name = "Player Title - " + player.name;
        Text playerTitleText = playerTitle.GetComponent<Text>();
        playerTitleText.text = player.name;
        playerTitleText.transform.SetParent(slotTransform);

        GameObject playerObject = new GameObject(player.name);
        playerObject.AddComponent<LayoutElement>();
        //playerObject.AddComponent<HorizontalLayoutGroup>();
        playerObject.transform.SetParent(slotTransform);
        playerTitleText.transform.SetParent(playerObject.transform);

        players.Add(player, playerObject);

        if (player != gameManager.gamePlayerManager.controlledPlayer)
        {
            PlayerAI playerAI = playerObject.AddComponent<PlayerAI>();
            playerAI.setPlayer(player);

            playerAIs.Add(player, playerAI);
        }
        else
        {
            PlayerCardController playerCardController = GameObject.FindObjectOfType<PlayerCardController>();
            playerCardController.setPlayer(player);
        }

        generatePointsBar(player, slotTransform);
    }

    // Callback for when a round changes
    public void onRoundChanged(Round round)
    {
        if (this.currentRound != null)
        {
            this.currentRound.unregisterCurrentTurnChangedCallback(onTurnChanged);
        }

        currentRound = round;

        if (currentRound != null)
        {
            currentRound.registerCurrentTurnChangedCallback(onTurnChanged);
        }
    }

    // Callback to let players know when its their turn
    public void onTurnChanged(Turn turn)
    {
        if (turn == null) return;   //This shouldn't ever be null, but...

        Debug.Log("TURN | Player: " + turn.player.name + " | Phase: " + turn.phase);

        if (turn.player != gameManager.gamePlayerManager.controlledPlayer && playerAIs.ContainsKey(turn.player))
        {
            playerAIs[turn.player].setPlayerTurn();
        }
    }

    // Generates a points bar for passed player as child of passed transform
    private void generatePointsBar(Player player, Transform playerTransform)
    {
        GameObject playerPointsBar = GameObject.Instantiate(pointsBarPrefab);

        playerPointsBar.name = player.name + " Points Bar";
        playerPointsBar.transform.SetParent(playerTransform);
        PointBarController pointBarController = playerPointsBar.AddComponent<PointBarController>();
        pointBarController.setPlayer(player);
    }
}
