﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotionObjectManager : MonoBehaviour
{
    // Prefabs
    private GameObject communityPotionPrefab;
    private GameObject personalPotionPrefab;

    // References
    private GameManager gameManager;
    private CanvasManager canvasManager;

    // Attributes
    private Dictionary<Player, GameObject> playerPotions;
    private GameObject communityPotion;
    private GameObject communityPotionInfoPanel;
    private GameObject playerPotionInfoPanel;
    private Player activePlayer;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
        canvasManager = GameObject.FindObjectOfType<CanvasManager>();

        communityPotionPrefab = (GameObject)Resources.Load("Prefabs/CommunityPotion");
        if (communityPotionPrefab == null) Debug.LogError("PotionPrefab failed to load");

        personalPotionPrefab = (GameObject)Resources.Load("Prefabs/PersonalPotion");
        if (personalPotionPrefab == null) Debug.LogError("PotionPrefab failed to load");

        communityPotionInfoPanel = GameObject.Find("CommunityPotionInfoPanel");
        if (communityPotionInfoPanel == null) Debug.LogError("CommunityPotionInfoPanel not found");

        playerPotionInfoPanel = GameObject.Find("PlayerPotionInfoPanel");
        if (playerPotionInfoPanel == null) Debug.LogError("PlayerPotionInfoPanel not found");
        playerPotionInfoPanel.SetActive(false);
    }

    // Generates the potion objects we'll use
    public void generatePotionObjects()
    {
        generateCommunityPotion();
        generatePlayerPotions();
    }

    // Generates the community potion object
    private void generateCommunityPotion()
    {
        communityPotion = GameObject.Instantiate(communityPotionPrefab);
        communityPotion.transform.SetParent(canvasManager.communityPotionTransform);
        communityPotion.transform.localPosition = new Vector3(0,0,0);
        communityPotion.name = "Community Potion";

        gameManager.gameRoundManager.registerRoundChangedCallback(onRoundChanged);
    }

    // Generate potion objects for each player
    private void generatePlayerPotions()
    {
        playerPotions = new Dictionary<Player, GameObject>();

        foreach (Player player in gameManager.gamePlayerManager.getPlayers())
        {
            GameObject personalPotion = GameObject.Instantiate(personalPotionPrefab);
            personalPotion.transform.SetParent(gameManager.playerObjectManager.getPlayerObject(player).transform);
            personalPotion.transform.SetAsFirstSibling();
            personalPotion.name = player.name + "'s Potion";
            playerPotions.Add(player, personalPotion);

            player.registerPotionChangedCallback(onPotionChanged);
        }
    }

    // Callback for when a new round begings
    public void onRoundChanged(Round round)
    {
        communityPotion.GetComponent<PotionController>().setPotion(round.communityPotion);
        communityPotion.AddComponent<Drop>();

        communityPotionInfoPanel.GetComponent<PotionInfoController>().setPotion(round.communityPotion);
        playerPotionInfoPanel.SetActive(false);

        round.registerCurrentTurnChangedCallback(onTurnChanged);
    }

    // Callback for when the player changes their potion
    public void onPotionChanged(Player player)
    {
        playerPotions[player].GetComponent<PotionController>().setPotion(player.potion);
    }

    // Callback for when a new turn begins
    public void onTurnChanged(Turn turn)
    {
        if (turn == null)
        {
            returnPotionToPlayer();
            return;
        }
        else if (turn.phase == 2)
        {
            returnPotionToPlayer();
            activePlayer = turn.player;
            activatePlayerPotion();

            GameObject.Destroy(communityPotion.GetComponent<Drop>());
        }
    }

    private void activatePlayerPotion()
    {
        playerPotionInfoPanel.SetActive(true);

        GameObject activePotion = playerPotions[activePlayer];

        activePotion.GetComponent<PotionController>().setDesiredLocation(canvasManager.playerPotionTransform);
        //activePotion.GetComponent<PotionController>().setDesiredScale(1.7f);

        playerPotionInfoPanel.GetComponent<PotionInfoController>().setPotion(activePlayer.potion);

        if (activePlayer == gameManager.gamePlayerManager.controlledPlayer)
        {
            Drop drop = activePotion.AddComponent<Drop>();
            drop.setAsPersonalPotion();
        }
    }

    private void returnPotionToPlayer()
    {
        if (activePlayer != null)
        {
            GameObject activePotion = playerPotions[activePlayer];

            activePotion.GetComponent<PotionController>().setDesiredLocation(gameManager.playerObjectManager.getPlayerObject(activePlayer).transform);
            //activePotion.GetComponent<PotionController>().setDesiredScale(1f);

            Drop drop = activePotion.GetComponent<Drop>();
            if (drop != null)
            {
                GameObject.Destroy(drop);
            }
        }
    }
}
