﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngredientObjectManager : MonoBehaviour
{
    // Prefabs
    private GameObject ingredientCardPrefab;

    // References
    private GameManager gameManager;
    private CanvasManager canvasManager;

    // Attributes
    private Dictionary<Ingredient, GameObject> ingredients;
    private Dictionary<string, Sprite> ingredientSprites;
    private Dictionary<IElement, Sprite> elementSprites;

    private Potion currentPotion;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
        canvasManager = GameObject.FindObjectOfType<CanvasManager>();

        ingredientCardPrefab = (GameObject)Resources.Load("Prefabs/IngredientCard");
        if (ingredientCardPrefab == null) Debug.LogError("IngredientCard failed to load");

        loadIngredientSprites();
        loadElementSprites();

        ingredients = new Dictionary<Ingredient, GameObject>();

        gameManager.gamePlayerManager.registerPlayerCreatedCallback(onPlayerCreated);
        gameManager.gamePlayerManager.registerControlledPlayerCreatedCallback(onControlledPlayerCreated);
        gameManager.gameRoundManager.registerRoundChangedCallback(onRoundChanged);
    }

    // Creates and ingredient card from prefab and updates it to match the ingredient info
    public GameObject createIngredientCard(Ingredient ingredient)
    {
        GameObject ingredientCardObject = GameObject.Instantiate(ingredientCardPrefab);
        ingredientCardObject.name = ingredient.name;

        ingredientCardObject.AddComponent<CanvasGroup>();

        Image ingredientImage = ingredientCardObject.transform.Find("IngredientImage").gameObject.GetComponent<Image>();
        ingredientImage.sprite = getIngredientSprite(ingredient.name);

        Text nameText = ingredientCardObject.transform.Find("NameText").gameObject.GetComponent<Text>();

        GameObject attributePanel = ingredientCardObject.transform.Find("AttributePanel").gameObject;

        Text tasteText = attributePanel.transform.Find("TasteText").gameObject.GetComponent<Text>();
        Text colorText = attributePanel.transform.Find("ColorText").gameObject.GetComponent<Text>();
        Text chargeText = attributePanel.transform.Find("ChargeText").gameObject.GetComponent<Text>();

        Image elementPanel = attributePanel.transform.Find("ElementPanel").gameObject.GetComponent<Image>();

        nameText.text = ingredient.name;
        tasteText.text = ingredient.taste.ToString();
        colorText.text = ingredient.color.ToString();

        string chargeString = "";

        switch (ingredient.charge)
        {
            case -3:
                chargeString = "- - -";
                break;
            case -2:
                chargeString = "- -";
                break;
            case -1:
                chargeString = "-";
                break;
            case 0:
                chargeString = "0";
                break;
            case 1:
                chargeString = "+";
                break;
            case 2:
                chargeString = "+ +";
                break;
            case 3:
                chargeString = "+ + +";
                break;
        }

        chargeText.text = chargeString;

        if (ingredient.element != IElement.None)
        {
            for (int i = 0; i < ingredient.elementCount; i++)
            {
                GameObject elementIconObject = new GameObject(ingredient.element + "Icon");

                Image elementIconImage = elementIconObject.AddComponent<Image>();
                elementIconImage.sprite = elementSprites[ingredient.element];

                elementIconObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 16);
                elementIconObject.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 16);

                elementIconObject.transform.SetParent(elementPanel.transform);
            }
        }

        Image ingredientCardBackgroundImage = ingredientCardObject.transform.Find("BackgroundImage").gameObject.GetComponent<Image>();
        ingredientCardBackgroundImage.color = ColorUtils.getColor(ingredient.color);

        ingredients.Add(ingredient, ingredientCardObject);

        return ingredientCardObject;
    }

    // To be called whenever an ingredient card is destroyed
    public void destroyIngredientCard(Ingredient ingredient)
    {
        if (ingredients.ContainsKey(ingredient))
        {
            Debug.Log("Destroying ingredient: " + ingredient.name);

            GameObject.Destroy(ingredients[ingredient]);
            ingredients.Remove(ingredient);
        }
    }

    // Callback for when a player-controlled player is created
    public void onControlledPlayerCreated(Player player)
    {
        player.registerIngredientAddedCallback(onIngredientAddedPlayer);
    }

    // Callback for when any player is created
    public void onPlayerCreated(Player player)
    {
        player.registerIngredientRemovedCallback(destroyIngredientCard);
    }

    // Callback for when an ingredient has been added to a player's hand
    public void onIngredientAddedPlayer(Ingredient ingredient)
    {
        //Debug.Log("Adding " + ingredient.name + " card to player inventory.");

        GameObject ingredientCardObject = createIngredientCard(ingredient);
        ingredientCardObject.transform.SetParent(canvasManager.playerIngredientsTransform);

        Drag drag = ingredientCardObject.AddComponent<Drag>();
        drag.setIngrediet(ingredient);
    }

    // Callback for when an ingredient has been added to the community potion
    public void onIngredientAddedPotion(Ingredient ingredient)
    {
        Debug.Log("Adding " + ingredient.name + " card to potion inventory.");

        GameObject ingredientCardObject = createIngredientCard(ingredient);
        ingredientCardObject.transform.SetParent(canvasManager.potionIngredientsTransform);
    }

    // Callback for when a new round begins
    public void onRoundChanged(Round round)
    {
        if (currentPotion != null)
        {
            foreach (Ingredient ingredient in currentPotion.getIngredients())
            {
                destroyIngredientCard(ingredient);
            }
        }

        currentPotion = round.communityPotion;

        currentPotion.registerIngredientAddedCallback(onIngredientAddedPotion);
    }

    // Spawns a card object that moves to appropriate potion location and completes turn for player (AI only)
    public void playAICard(Player player, Ingredient ingredient, int phase)
    {
        GameObject ingredientCardObject = createIngredientCard(ingredient);
        ingredientCardObject.transform.SetParent(canvasManager.ingredientsCanvasTransform);
        ingredientCardObject.transform.position = gameManager.playerObjectManager.getPlayerObject(player).transform.position;

        IngredientController ingredientController = ingredientCardObject.AddComponent<IngredientController>();

        ingredientController.setIngredientInfo(player, ingredient);

        if (phase == 1)
        {
            ingredientController.setTarget(canvasManager.communityPotionTransform);
            ingredientController.registerCardArrivedCallback(gameManager.addIngredientToComunityPotion);
        }
        else if (phase == 2)
        {
            ingredientController.setTarget(canvasManager.playerPotionTransform);
            ingredientController.registerCardArrivedCallback(gameManager.addIngredientToPersonalPotion);
        }
    }

    // Accesses the sprite based on the passed ingredient name, returns a default if not present
    private Sprite getIngredientSprite(string name)
    {
        if (ingredientSprites.ContainsKey(name))
        {
            return ingredientSprites[name];
        }
        else if (ingredientSprites.ContainsKey("default"))
        {
            return ingredientSprites["default"];
        }

        return null;
    }

    // Loads all element icons and puts them in a dictionary by IElement
    private void loadElementSprites()
    {
        elementSprites = new Dictionary<IElement, Sprite>();
        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Elements/");

        foreach (Sprite sprite in sprites)
        {
            IElement element = IElement.None;

            switch (sprite.name)
            {
                case "air":
                    element = IElement.Air;
                    break;
                case "earth":
                    element = IElement.Earth;
                    break;
                case "fire":
                    element = IElement.Fire;
                    break;
                case "water":
                    element = IElement.Water;
                    break;
            }

            if (element == IElement.None) Debug.LogError("Recheck sprite names for elements");

            elementSprites.Add(element, sprite);
        }
    }

    // Loads all ingredient images and puts them in a dictionary by ingredient name
    private void loadIngredientSprites()
    {
        ingredientSprites = new Dictionary<string, Sprite>();

        Sprite[] sprites = Resources.LoadAll<Sprite>("Sprites/Ingredients/");

        foreach (Sprite sprite in sprites)
        {
            ingredientSprites.Add(sprite.name, sprite);
        }
    }
}
