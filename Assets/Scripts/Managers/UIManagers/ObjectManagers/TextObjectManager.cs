﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextObjectManager : MonoBehaviour 
{
    // Prefabs
    private GameObject gameTextPrefab;

    // References
    private GameManager gameManager;
    private CanvasManager canvasManager;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
        canvasManager = GameObject.FindObjectOfType<CanvasManager>();

        gameTextPrefab = (GameObject)Resources.Load("Prefabs/GameText");
        if (gameTextPrefab == null) Debug.LogError("gameTextPrefab failed to load");

        gameManager.gamePlayerManager.registerPlayerCreatedCallback(onPlayerCreated);
        gameManager.gameRoundManager.registerRoundChangedCallback(onRoundBegin);
        gameManager.gameRoundManager.registerWinnerDecidedCallback(onWinnerDecided);
    }

    public void onRoundBegin(Round round)
    {
        spawnText("Beginning New Round", ColorUtils.getColor(IColor.Green), getScreenCenter(), 50);
        round.registerCurrentTurnChangedCallback(onTurnChanged);
    }

    public void onTurnChanged(Turn turn)
    {
        if(turn != null && turn.player == gameManager.gamePlayerManager.controlledPlayer)
        {
            switch (turn.phase)
            {
                case 1:
                    spawnText("Add an ingredient to the main potion.", ColorUtils.getColor(IColor.Green), getScreenCenter(125), 40);
                    break;
                case 2:
                    spawnText("Add an ingredient to your potion.", ColorUtils.getColor(IColor.Green), getScreenCenter(125), 40);
                    break;
                case 3:
                    spawnText("Drink your potion.", ColorUtils.getColor(IColor.Green), getScreenCenter(125), 40);
                    break;
            }

        }
    }

    public void onPlayerCreated(Player player)
    {
        player.registerPointsChangedCallback(onPlayerDamaged);
    }

    public void onPlayerDamaged(Player player, int damage)
    {
        Vector3 location = gameManager.playerObjectManager.getPlayerObject(player).GetComponent<RectTransform>().position;
        Color color = ColorUtils.getColor(damage > 0 ? IColor.Red : IColor.Green);

        string damageString = (damage > 0 ? "-" : "") + damage;

        spawnText(damageString, color, location, 60);
    }

    public void onWinnerDecided(Player player)
    {
        spawnText(player.name + " wins!!!", ColorUtils.getColor(IColor.Green), getScreenCenter(), 60);
    }

    // Spawns a text object at desired location
    private Text spawnText(string content, Color color, Vector3 location, int size = 30)
    {
        GameObject gameText = GameObject.Instantiate(gameTextPrefab);
        gameText.transform.SetParent(canvasManager.gameTextCanvas);
        gameText.GetComponent<RectTransform>().position = location;

        Text text = gameText.GetComponent<Text>();
        text.text = content;
        text.color = color;
        text.fontSize = size;

        return text;
    }

    private Vector3 getScreenCenter(int yOffset = 0)
    {
        float x = Screen.width / 2f;
        float y = (Screen.height / 2f) + yOffset;

        return new Vector3(x, y, 0);
    }
}
