﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientController : MonoBehaviour 
{
    // Attributes
    private Ingredient ingredient;
    private Player player;

    private Vector3 startingLocation;
    private Vector3 desiredLocation;
    private float locationPercent;
    private float speed = 0.02f;

    private float wait;

    // Callback
    private Action<Player, Ingredient> onCardArrivedCallback;

    // Use this for initialization
    void Start ()
    {
        startingLocation = transform.position;
        locationPercent = 0f;

        wait = 2f;
    }
	
	// Update is called once per frame
	void Update () 
	{
        if (wait >= 0)
        {
            wait -= Time.deltaTime;
        }
        else
        {
            locationPercent += Time.deltaTime + speed;

            if (locationPercent >= 1)
            {
                if (onCardArrivedCallback != null) onCardArrivedCallback(player, ingredient);
                GameObject.Destroy(this.gameObject);
            }
            else
            {
                transform.position = Vector3.Lerp(startingLocation, desiredLocation, locationPercent);
            }
        }
	}

    public void setIngredientInfo(Player player, Ingredient ingredient)
    {
        this.ingredient = ingredient;
        this.player = player;
    }

    public void setTarget(Transform target)
    {
        desiredLocation = target.position;
    }

    // Callbacks for when the ingredient card arrives at its destination
    public void registerCardArrivedCallback(Action<Player, Ingredient> callback)
    {
        onCardArrivedCallback += callback;
    }

    public void unregisterCardArrivedCallback(Action<Player, Ingredient> callback)
    {
        onCardArrivedCallback -= callback;
    }
}
