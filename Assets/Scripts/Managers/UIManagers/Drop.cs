﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Drop : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    private GameManager gameManager;
    private bool community = true;

    void OnEnable()
    {
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //Debug.Log("OnPointerEnter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //Debug.Log("OnPointerExit");
    }

    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("OnDrop");

        Drag drag = eventData.pointerDrag.GetComponent<Drag>();
        addIngredientToPotion(drag.ingredient);
        drag.removeGhostImage();
    }

    public void addIngredientToPotion(Ingredient ingredient)
    {
        Player player = gameManager.gamePlayerManager.controlledPlayer;

        if (community == true)
        {
            gameManager.addIngredientToComunityPotion(player, ingredient);
        }
        else
        {
            gameManager.addIngredientToPersonalPotion(player, ingredient);
        }
    }

    public void setAsPersonalPotion()
    {
        community = false;
    }
}
