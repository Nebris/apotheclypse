﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Ingredient ingredient { get; protected set; }

    private Transform ingredientsCanvas = null;
    private Transform playerIngredients = null;
    private GameObject ghost = null;
    private int siblingIndex;

    void Start()
    {
        ingredientsCanvas = GameObject.Find("IngredientsCanvas").transform;
        playerIngredients = transform.parent;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("OnBeginDrag");
        generateGhostImage();
        transform.SetParent(ingredientsCanvas);

        GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("OnEndDrag");
        removeGhostImage();
        transform.SetParent(playerIngredients);
        transform.SetSiblingIndex(siblingIndex);

        GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    private void generateGhostImage()
    {
        ghost = new GameObject();
        siblingIndex = transform.GetSiblingIndex();

        ghost.transform.SetParent(playerIngredients);
        ghost.transform.SetSiblingIndex(siblingIndex);

        ghost.AddComponent<CanvasRenderer>();
        CanvasGroup canvasGroup = ghost.AddComponent<CanvasGroup>();
        canvasGroup.blocksRaycasts = false;

        Image image = ghost.AddComponent<Image>();
        Color newColor = gameObject.GetComponent<Image>().color;
        newColor.a = .2f;
        image.color = newColor;
    }

    public void removeGhostImage()
    {
        GameObject.Destroy(ghost);
    }

    public void setIngrediet(Ingredient ingredient)
    {
        this.ingredient = ingredient;
    }
}
