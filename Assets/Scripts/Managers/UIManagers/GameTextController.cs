﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTextController : MonoBehaviour 
{
    // Attributes
    private float staticDuration = 2f;
    private float decayDuration = 1f;
    private float decayDurationInitialValue;

    // References
    private Text text;

    // Use this for initialization
    void Start () 
	{
        text = GetComponent<Text>();
        decayDurationInitialValue = decayDuration;
    }
	
	// Update is called once per frame
	void Update () 
	{
		if(staticDuration > 0)
        {
            staticDuration -= Time.deltaTime;
        }
        else if(decayDuration > 0)
        {
            decayDuration -= Time.deltaTime;
            text.color = new Color(text.color.r, text.color.g, text.color.b, decayDuration/decayDurationInitialValue);
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }
	}
}
