﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCardController : MonoBehaviour 
{
    // References
    private Text nameText;
    private Text likedTasteText;
    private Text disLikedtasteText;
    private Text colorText;
    private Text chargeText;
    private Text elementText;
    
    // Attributes
    private Player player;

    // Use this for initialization
    void OnEnable ()
    {
        nameText = transform.Find("PlayerNamePanel").Find("PlayerNameValue").GetComponent<Text>();
        likedTasteText = transform.Find("LikedTastePanel").Find("LikedTasteValue").GetComponent<Text>();
        disLikedtasteText = transform.Find("DisikedTastePanel").Find("DislikedTasteValue").GetComponent<Text>();
        colorText = transform.Find("ColorPanel").Find("ColorValue").GetComponent<Text>();
        chargeText = transform.Find("ChargePanel").Find("ChargeValue").GetComponent<Text>();
        elementText = transform.Find("ElementPanel").Find("ElementValue").GetComponent<Text>();

        if (nameText == null) Debug.LogError("Cannot find nameText.");
        if (likedTasteText == null) Debug.LogError("Cannot find tasteText.");
        if (disLikedtasteText == null) Debug.LogError("Cannot find disLikedtasteText.");
        if (colorText == null) Debug.LogError("Cannot find colorText.");
        if (chargeText == null) Debug.LogError("Cannot find chargeText.");
        if (elementText == null) Debug.LogError("Cannot find elementText.");
    }

    // Changes the potion we keep track of
    public void setPlayer(Player player)
    {
        this.player = player;
        updatePlayerCard();
    }

    // Changes the values of the texts to match the current player
    private void updatePlayerCard()
    {
        if (player == null) return;

        nameText.text = player.character.name;
        likedTasteText.text = player.character.tasteLike.ToString();
        disLikedtasteText.text = player.character.tasteDislike.ToString();
        colorText.text = player.character.color.ToString();
        chargeText.text = player.character.charge.ToString();
        elementText.text = player.character.element.ToString();
    }
}
