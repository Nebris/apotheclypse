﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PotionInfoController : MonoBehaviour 
{
    // References
    private Text tasteText;
    private Text colorText;
    private Text chargeText;
    private Text elementText;
    private Text earthText;
    private Text fireText;
    private Text airText;
    private Text waterText;

    // Attributes
    private Potion potion;

    // Use this for initialization
    void OnEnable()
    {
        tasteText = transform.Find("Potion Taste Panel").Find("Potion Taste Value").GetComponent<Text>();
        colorText = transform.Find("Potion Color Panel").Find("Potion Color Value").GetComponent<Text>();
        chargeText = transform.Find("Potion Charge Panel").Find("Potion Charge Value").GetComponent<Text>();
        elementText = transform.Find("Potion Element Panel").Find("Potion Element Value").GetComponent<Text>();
        earthText = transform.Find("Potion Earth Element Panel").Find("Potion Earth Element Value").GetComponent<Text>();
        fireText = transform.Find("Potion Fire Element Panel").Find("Potion Fire Element Value").GetComponent<Text>();
        airText = transform.Find("Potion Air Element Panel").Find("Potion Air Element Value").GetComponent<Text>();
        waterText = transform.Find("Potion Water Element Panel").Find("Potion Water Element Value").GetComponent<Text>();

        if (tasteText == null) Debug.LogError("Cannot find tasteText.");
        if (colorText == null) Debug.LogError("Cannot find colorText.");
        if (chargeText == null) Debug.LogError("Cannot find chargeText.");
        if (elementText == null) Debug.LogError("Cannot find elementText.");
        if (earthText == null) Debug.LogError("Cannot find earthText.");
        if (fireText == null) Debug.LogError("Cannot find fireText.");
        if (airText == null) Debug.LogError("Cannot find airText.");
        if (waterText == null) Debug.LogError("Cannot find waterText.");
    }

    // Callback for when the potion has been altered
    public void onPotionChanged(Ingredient ingredient)
    {
        if (potion == null) return;

        tasteText.text = potion.getPotionTaste().ToString();
        colorText.text = potion.getPotionColor().ToString();
        chargeText.text = potion.getPotionCharge().ToString();
        elementText.text = potion.getPotionElement().ToString();

        potion.updateElements();
        earthText.text = potion.getElementCount(IElement.Earth).ToString();
        fireText.text = potion.getElementCount(IElement.Fire).ToString();
        airText.text = potion.getElementCount(IElement.Air).ToString();
        waterText.text = potion.getElementCount(IElement.Water).ToString();
    }

    // Changes the potion we keep track of
    public void setPotion(Potion potion)
    {
        if (this.potion != null) this.potion.unregisterIngredientAddedCallback(onPotionChanged);

        this.potion = potion;
        potion.registerIngredientAddedCallback(onPotionChanged);
        onPotionChanged(null);
    }
}
