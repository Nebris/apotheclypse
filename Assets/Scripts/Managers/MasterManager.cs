﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MasterManager : MonoBehaviour
{
    // Singleton
    public static MasterManager Instance { get; protected set; }

    public IngredientManager ingredientManager { get; protected set; }
    public CharacterManager characterManager { get; protected set; }

    public GameInfo gameInfo { get; protected set; }

    public GameObject gameManagerObject;

    // Use this for initialization
    void Awake()
    {
        // Singletonize
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(gameObject);
            return;
        }

        ingredientManager = new IngredientManager();
        characterManager = new CharacterManager();

        gameInfo = new GameInfo();

        // Register scene change callback
        SceneManager.sceneLoaded += OnLevelLoaded;
    }

    // To be called whenever the scene changes.  Destroy and regenerate necessary managers for this scene
    private void OnLevelLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Level loaded: " + scene.name + "\tMode " + mode);

        deactivateGameManager();

        if (scene.name == "Game")
        {
            activateGameManager();
        }
    }

    private void activateGameManager()
    {
        gameManagerObject = new GameObject("Game Manager");
        gameManagerObject.transform.SetParent(this.transform);

        gameManagerObject.AddComponent<GameManager>();
    }

    private void deactivateGameManager()
    {
        GameObject.Destroy(gameManagerObject);
    }
}
