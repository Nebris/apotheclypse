﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameMode { Elimination, Quest }

public class GameInfo
{
    // Attributes
    public GameMode gameMode { get; protected set; }
    public int ingredients { get; protected set; }
    public int players { get; protected set; }
    public int points { get; protected set; }
    public bool isPlayerCardRandom { get; protected set; }

    //public CharacterCard characterCard = null;
    //public QuestCard questCard = null;

    // Constructor
    public GameInfo()
    {
        gameMode = GameMode.Elimination;

        players = 4;
        points = 10;
        ingredients = 5;

        isPlayerCardRandom = true;
    }

    // Sets gameMode
    public void setGameMode(GameMode gameMode)
    {
        this.gameMode = gameMode;
    }

    // Sets number of ingredients in player inventory
    public void setIngredients(int ingredients)
    {
        this.ingredients = ingredients;
    }

    // Sets whether or not a player's card is random (choose a character/quest)
    public void setPlayerCardRandom(bool isPlayerCardRandom)
    {
        this.isPlayerCardRandom = isPlayerCardRandom;
    }

    // Sets number of players in game
    public void setPlayers(int players)
    {
        this.players = players;
    }

    // Sets number of points for game (either HP or QP)
    public void setPoints(int points)
    {
        this.points = points;
    }

    // Function to log all attributes
    public void logGameInfo()
    {
        Debug.Log("GameMode: " + gameMode + " | Random Player Card: " + isPlayerCardRandom + " | Players: " + players +  " | Points: " + points + " | Ingredients: " + ingredients);
    }
}
