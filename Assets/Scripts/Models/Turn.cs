﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turn
{
    // Attributes
    public Player player { get; protected set; }
    public int phase { get; protected set; }

    // Constructor
    public Turn(Player player, int phase)
    {
        this.player = player;
        this.phase = phase;
    }
}
