﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Round
{
    // Attributes
    public Potion communityPotion { get; protected set; }
    public Turn currentTurn { get; protected set; }

    private List<Player> players;
    private List<Turn> turns;

    private int currentTurnIndex;

    // Callback
    private Action<Turn> currentTurnChangedCallback;
    private Action roundOverCallback;

    // Constructor
    public Round (List<Player> players)
    {
        this.players = new List<Player>(players);
        communityPotion = new Potion();
        turns = new List<Turn>();

        for (int i = 0; i < players.Count; i++)
        {
            turns.Add(new Turn(players[i], 1));
        }

        for (int i = players.Count - 1; i >= 0; i--)
        {
            turns.Add(new Turn(players[i], 2));
            turns.Add(new Turn(players[i], 3));
        }
    }

    public void begin()
    {
        currentTurnIndex = 0;
        currentTurn = turns[currentTurnIndex];

        if (currentTurnChangedCallback != null) currentTurnChangedCallback(currentTurn);
    }

    // Returns copy of players present in this round
    public List<Player> getPlayers()
    {
        return new List<Player>(players);
    }

    // Advances the turn
    public void nextTurn()
    {
        currentTurnIndex++;

        currentTurn = currentTurnIndex < turns.Count ? turns[currentTurnIndex] : null;
        
        if(currentTurn != null && currentTurn.phase == 2)
        {
            currentTurn.player.fillPotion(communityPotion);
        }

        if (currentTurnChangedCallback != null)
        {
            currentTurnChangedCallback(currentTurn);
        }

        if (currentTurn == null && roundOverCallback != null)
        {
            roundOverCallback();
        }
    }

    // Callback for when a new turn begins
    public void registerCurrentTurnChangedCallback(Action<Turn> callback)
    {
        currentTurnChangedCallback += callback;
    }

    public void unregisterCurrentTurnChangedCallback(Action<Turn> callback)
    {
        currentTurnChangedCallback -= callback;
    }

    // Callback for when round ends
    public void registerRoundOverCallback(Action callback)
    {
        roundOverCallback += callback;
    }

    public void unregisterRoundOverCallback(Action callback)
    {
        roundOverCallback -= callback;
    }
}
