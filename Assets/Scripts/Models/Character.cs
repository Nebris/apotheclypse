﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character
{
    // Attributes
    public string name { get; protected set; }
    public ITaste tasteLike { get; protected set; }
    public ITaste tasteDislike { get; protected set; }
    public IColor color { get; protected set; }
    public int charge { get; protected set; }
    public IElement element { get; protected set; }

    // Constructor
    public Character (string name, ITaste tasteLike, ITaste tasteDislike, IColor color, int charge, IElement element)
    {
        this.name = name;
        this.tasteLike = tasteLike;
        this.tasteDislike = tasteDislike;
        this.color = color;
        this.charge = charge;
        this.element = element;

        if (tasteLike == tasteDislike) Debug.LogError("Matching liked and disliked tastes.");
    }

    // Copy Constructor
    public Character(Character character)
    {
        this.name = character.name;
        this.tasteLike = character.tasteLike;
        this.tasteDislike = character.tasteDislike;
        this.color = character.color;
        this.charge = character.charge;
        this.element = character.element;
    }
}
