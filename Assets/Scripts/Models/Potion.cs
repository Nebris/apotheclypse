﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Potion 
{
    // Attributes
    private List<Ingredient> ingredients;
    private Dictionary<IElement, int> elements;

    // Callbacks
    private Action<Ingredient> ingredientAddedCallback;

    // Constructor
    public Potion ()
    {
        ingredients = new List<Ingredient>();
        updateElements();
    }

    // Copy Constructor
    public Potion (Potion potion)
    {
        ingredients = new List<Ingredient>(potion.ingredients);
        updateElements();
    }

    // Adds passed ingredient into the potion
    public void addIngredient(Ingredient ingredient)
    {
        ingredients.Add(ingredient);

        if (ingredientAddedCallback != null) ingredientAddedCallback(ingredient);
    }

    // Returns copy of list of ingredients this potion is composed of
    public List<Ingredient> getIngredients()
    {
        return new List<Ingredient>(ingredients);
    }

    // Returns a count of the passed element in this potion
    public int getElementCount(IElement element)
    {
        return elements[element];
    }

    // Calculates and returns the charge attribute of the potion
    public int getPotionCharge()
    {
        int charge = 0;

        for (int i = 0; i < ingredients.Count; i++)
        {
            charge += ingredients[i].charge;
        }

        return charge;
    }

    // Calculates and returns the color attribute of the potion
    public IColor getPotionColor()
    {
        IColor currentColor = IColor.White;

        for (int i = 0; i < ingredients.Count; i++)
        {
            currentColor = ColorUtils.getNextColor(currentColor, ingredients[i].color);
        }

        return currentColor;
    }

    // Calculates and returns the element attribute of the potion
    public IElement getPotionElement()
    {
        IElement currentElement = IElement.None;
        int currentCount = 0;

        updateElements();

        foreach (IElement element in elements.Keys)
        {
            if (element == IElement.None) continue;

            if (elements[element] > currentCount)
            {
                currentElement = element;
                currentCount = elements[element];
            }
            else if (elements[element] == currentCount)
            {
                currentElement = IElement.None;
            }
        }

        return currentElement;
    }

    // Calculates and returns the taste attribute of the potion
    public ITaste getPotionTaste()
    {
        if (ingredients.Count == 0) return ITaste.Bitter;

        return ingredients[ingredients.Count - 1].taste;
    }

    // Recalculates the total number of points for each element in the potion
    public void updateElements()
    {
        elements = new Dictionary<IElement, int>();

        foreach (IElement element in Enum.GetValues(typeof(IElement)))
        {
            elements.Add(element, 0);
        }

        for (int i = 0; i < ingredients.Count; i++)
        {
            elements[ingredients[i].element] += ingredients[i].elementCount;
        }
    }

    // Callbacks for when an ingredient is added to the potion
    public void registerIngredientAddedCallback(Action<Ingredient> callback)
    {
        ingredientAddedCallback += callback;
    }

    public void unregisterIngredientAddedCallback(Action<Ingredient> callback)
    {
        ingredientAddedCallback -= callback;
    }

    // Function to log potion attributes
    public void logPotionAttributes()
    {
        Debug.Log("POTION | Taste: " + getPotionTaste() + " | Color: " + getPotionColor() + " | Charge: " + getPotionCharge() + " | Element: " + getPotionElement());
    }
}
