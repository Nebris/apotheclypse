﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum IColor      { White, Black, Red, Orange, Yellow, Green, Blue, Purple }
public enum IElement    { None, Earth, Fire, Air, Water }
public enum ITaste      { Bitter, Salty, Sour, Spicy, Sweet }

public class Ingredient
{
    // Attributes
    public string name { get; protected set; }
    public ITaste taste { get; protected set; }
    public IColor color { get; protected set; }
    public int charge { get; protected set; }
    public IElement element { get; protected set; }
    public int elementCount { get; protected set; }

    // Constructor
    public Ingredient(string name, ITaste taste, IColor color, int charge, IElement element, int elementCount)
    {
        this.name = name;
        this.taste = taste;
        this.color = color;
        this.charge = charge;
        this.element = element;
        this.elementCount = elementCount;
    }

    // Copy Constructor
    public Ingredient(Ingredient ingredient)
    {
        this.name = ingredient.name;
        this.taste = ingredient.taste;
        this.color = ingredient.color;
        this.charge = ingredient.charge;
        this.element = ingredient.element;
        this.elementCount = ingredient.elementCount;
    }

    // Function to log all attributes
    public void logIngredient()
    {
        Debug.Log("INGREDIENT | Name: " + name + " | Taste: " + taste + " | Color: " + color + " | Charge: " + charge + " | Element: " + element + " | Count: " + elementCount);
    }
}
