﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Player
{
    // Attributes
    public string name { get; protected set; }
    public Character character { get; protected set; }
    public int points { get; protected set; }
    public Potion potion { get; protected set; }

    private List<Ingredient> ingredients;

    // Callbacks
    private Action<Player, int> pointsChangedCallback;
    private Action<Player> potionChangedCallback;
    private Action<Ingredient> ingredientAddedCallback;
    private Action<Ingredient> ingredientRemovedCallback;

    // Constructor
    public Player()
    {
        ingredients = new List<Ingredient>();
    }

    // Adds passed ingredient to player inventory
    public void addIngredient(Ingredient ingredient)
    {
        ingredients.Add(ingredient);

        if (ingredientAddedCallback != null) ingredientAddedCallback(ingredient);
    }

    public void assessPotionDamage()
    {
        int damage = 0;

        // TASTE
        ITaste taste = potion.getPotionTaste();

        if (taste == character.tasteLike)
        {
            Debug.Log(name + " is shielded from 1 damage from the taste attribute of the potion.");
            damage -= 1;
        }
        else if (taste == character.tasteDislike)
        {
            Debug.Log(name + " takes 1 damage from the taste attribute of the potion.");
            damage += 1;
        }
        else
        {
            Debug.Log(name + " takes no damage from the taste attribute of the potion.");
        }

        // COLOR
        IColor color = potion.getPotionColor();

        if (color == IColor.Black)
        {
            Debug.Log(name + " takes 2 damage from the color attribute of the potion.");
            damage += 2;
        }
        else if (ColorUtils.isHarmless(character.color, color) == false)
        {
            Debug.Log(name + " takes 1 damage from the color attribute of the potion.");
            damage += 1;
        }
        else
        {
            Debug.Log(name + " takes no damage from the color attribute of the potion.");
        }

        // CHARGE
        int charge = potion.getPotionCharge();
        int difference = Mathf.Abs(charge - character.charge);

        if (difference == 0)
        {
            Debug.Log(name + " is shielded from 1 damage from the charge attribute of the potion.");
            damage -= 1;
        }
        else if (difference < 2)
        {
            Debug.Log(name + " takes no damage from the charge attribute of the potion.");
            damage += 0;
        }
        else if (difference < 4)
        {
            Debug.Log(name + " takes 1 damage from the charge attribute of the potion.");
            damage += 1;
        }
        else
        {
            Debug.Log(name + " takes 2 damage from the charge attribute of the potion.");
            damage += 2;
        }

        // ELEMENT
        IElement element = potion.getPotionElement();

        if (character.element == element || element == IElement.None)
        {
            Debug.Log(name + " takes no damage from the element attribute of the potion.");
        }
        else
        {
            Debug.Log(name + " takes 1 damage from the element attribute of the potion.");
            damage += 1;
        }

        // TOTAL
        damage = Math.Max(0, damage);

        setPoints(Math.Max(0, points - damage));

        Debug.Log(name + " takes " + damage + " total damage from the potion and has " + points + " health remaining.");
    }

    // Destroys the player's current potion
    public void emptyPotion()
    {
        this.potion = null;

        if (potionChangedCallback != null) potionChangedCallback(this);
    }

    // Creates new potion based off of passed potion and assigns it to player
    public void fillPotion(Potion potion)
    {
        this.potion = new Potion(potion);

        if (potionChangedCallback != null) potionChangedCallback(this);
    }

    // Returns the number of ingredients in inventory
    public int getIngredientsCount()
    {
        return ingredients.Count;
    }

    // Returns a copy of the list containing this player's inventory of ingredients
    public List<Ingredient> getIngredients()
    {
        return new List<Ingredient>(ingredients);
    }

    // Removes passed ingredient from player inventory
    public void removeIngredient(Ingredient ingredient)
    {
        ingredients.Remove(ingredient);

        if (ingredientRemovedCallback != null) ingredientRemovedCallback(ingredient);
    }

    // Sets the character this player is playing as (in elimination mode)
    public void setCharacter(Character character)
    {
        this.character = character;
    }

    // Sets the name of this player
    public void setName(string name)
    {
        this.name = name;
    }

    // Sets the current value of points a player has
    public void setPoints(int points)
    {
        int difference = this.points - points;

        this.points = points;

        if (pointsChangedCallback != null) pointsChangedCallback(this, difference);
    }

    // Callbacks for when points change
    public void registerPointsChangedCallback(Action<Player, int> callback)
    {
        pointsChangedCallback += callback;
    }

    public void unregisterPointsChangedCallback(Action<Player, int> callback)
    {
        pointsChangedCallback -= callback;
    }

    // Callbacks for when potions change
    public void registerPotionChangedCallback(Action<Player> callback)
    {
        potionChangedCallback += callback;
    }

    public void unregisterPotionChangedCallback(Action<Player> callback)
    {
        potionChangedCallback -= callback;
    }

    // Callbacks for when ingredients are added
    public void registerIngredientAddedCallback(Action<Ingredient> callback)
    {
        ingredientAddedCallback += callback;
    }

    public void unregisterIngredientAddedCallback(Action<Ingredient> callback)
    {
        ingredientAddedCallback -= callback;
    }

    // Callbacks for when ingredients are added
    public void registerIngredientRemovedCallback(Action<Ingredient> callback)
    {
        ingredientRemovedCallback += callback;
    }

    public void unregisterIngredientRemovedCallback(Action<Ingredient> callback)
    {
        ingredientRemovedCallback -= callback;
    }

    // Function to log current inventory of ingredients
    public void logIngredients()
    {
        string log = "| ";

        foreach (Ingredient ingredient in ingredients)
        {
            log += (ingredient.name + " | ");
        }

        Debug.Log(log);
    }
}
