﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorUtils
{
    public static Color getColor(IColor iColor)
    {
        Color color = Color.white;

        switch (iColor)
        {
            case IColor.White:
                color = new Color(204 / 255f, 204 / 255f, 204 / 255f, 1);
                break;
            case IColor.Red:
                color = new Color(204 / 255f, 0 / 255f, 0 / 255f, 1);
                break;
            case IColor.Orange:
                color = new Color(204 / 255f, 100 / 255f, 0 / 255f, 1);
                break;
            case IColor.Yellow:
                color = new Color(204 / 255f, 204 / 255f, 0 / 255f, 1);
                break;
            case IColor.Green:
                color = new Color(0 / 255f, 204 / 255f, 0 / 255f, 1);
                break;
            case IColor.Blue:
                color = new Color(0 / 255f, 100 / 255f, 204/ 255f, 1);
                break;
            case IColor.Purple:
                color = new Color(150 / 255f, 0 / 255f, 204 / 255f, 1);
                break;
            case IColor.Black:
                color = new Color(102 / 255f, 102 / 255f, 102 / 255f, 1);
                break;
        }

        return color;
    }

    // Returns true if a potion will not harm the player
    public static bool isHarmless(IColor playerColor, IColor potionColor)
    {
        if (potionColor == IColor.White)
        {
            return true;
        }

        if (potionColor == playerColor)
        {
            return true;
        }

        if (playerColor == IColor.Red && hasRed(potionColor))
        {
            return true;
        }

        if (playerColor == IColor.Yellow && hasYellow(potionColor))
        {
            return true;
        }

        if (playerColor == IColor.Blue && hasBlue(potionColor))
        {
            return true;
        }

        return false;
    }

    // Returns true if the passed color has red in it
    public static bool hasRed(IColor color)
    {
        return (color == IColor.Red || color == IColor.Orange || color == IColor.Purple);
    }

    // Returns true if the passed color has yellow in it
    public static bool hasYellow(IColor color)
    {
        return (color == IColor.Yellow || color == IColor.Orange || color == IColor.Green);
    }

    // Returns true if the passed color has blue in it
    public static bool hasBlue(IColor color)
    {
        return (color == IColor.Blue || color == IColor.Green || color == IColor.Purple);
    }

    // Calculates the color the potion will be based on the current color and the ingredient color
    public static IColor getNextColor(IColor currentColor, IColor ingredientColor)
    {
        IColor nextColor = currentColor;

        //Return if they are the same
        if (currentColor == ingredientColor)
        {
            return ingredientColor;
        }

        // Everything overwrites white, white and black overwrites everything
        if (currentColor == IColor.White || ingredientColor == IColor.White || ingredientColor == IColor.Black)
        {
            return ingredientColor;
        }

        if (currentColor == IColor.Red)
        {
            if (ingredientColor == IColor.Yellow || ingredientColor == IColor.Orange)
            {
                nextColor = IColor.Orange;
            }
            else if (ingredientColor == IColor.Blue || ingredientColor == IColor.Purple)
            {
                nextColor = IColor.Purple;
            }
            else if (ingredientColor == IColor.Green)
            {
                nextColor = IColor.Black;
            }
        }

        if (currentColor == IColor.Yellow)
        {
            if (ingredientColor == IColor.Red || ingredientColor == IColor.Orange)
            {
                nextColor = IColor.Orange;
            }
            else if (ingredientColor == IColor.Blue || ingredientColor == IColor.Green)
            {
                nextColor = IColor.Green;
            }
            else if (ingredientColor == IColor.Purple)
            {
                nextColor = IColor.Black;
            }
        }

        if (currentColor == IColor.Blue)
        {
            if (ingredientColor == IColor.Red || ingredientColor == IColor.Purple)
            {
                nextColor = IColor.Purple;
            }
            else if (ingredientColor == IColor.Yellow || ingredientColor == IColor.Green)
            {
                nextColor = IColor.Green;
            }
            else if(ingredientColor == IColor.Orange)
            {
                nextColor = IColor.Black;
            }
        }

        if (currentColor == IColor.Orange && (ingredientColor == IColor.Blue || ingredientColor == IColor.Green || ingredientColor == IColor.Purple))
        {
            nextColor = IColor.Black;
        }

        if (currentColor == IColor.Green && (ingredientColor == IColor.Red || ingredientColor == IColor.Orange || ingredientColor == IColor.Purple))
        {
            nextColor = IColor.Black;
        }

        if (currentColor == IColor.Purple && (ingredientColor == IColor.Yellow || ingredientColor == IColor.Green || ingredientColor == IColor.Orange))
        {
            nextColor = IColor.Black;
        }

        return nextColor;
    }
}
