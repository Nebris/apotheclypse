﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAI : MonoBehaviour 
{
    // References
    public GameManager gameManager;

    //Attributes
    private Player player;
    private bool turn;

    private float wait = 0f;

	void OnEnable() 
	{
        gameManager = MasterManager.Instance.gameManagerObject.GetComponent<GameManager>();
        turn = false;
    }

    void Update()
    {
        if (turn == true)
        {
            if (wait >= 0)
            {
                wait -= Time.deltaTime;
            }
            else
            {
                turn = false;
                int phase = gameManager.gameRoundManager.currentRound.currentTurn.phase;

                if (phase == 1 || phase == 2)
                {
                    gameManager.ingredientObjectManager.playAICard(player, getRandomIngredient(), phase);
                }
                else if (phase == 3)
                {
                    gameManager.drinkPlayerPotion(player);
                }
            }
        }
    }

    // Assigns player to this AI script
    public void setPlayer(Player player)
    {
        this.player = player;
    }

    // Sets player turn to true
    public void setPlayerTurn()
    {
        turn = true;
        wait = 1f;
    }

    // Retrieves random ingredient from player's inventory
    private Ingredient getRandomIngredient()
    {
        List<Ingredient> ingredients = player.getIngredients();

        return ingredients[(int)Random.Range(0, ingredients.Count)];
    }
}
